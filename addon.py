import sys
import urllib.request, urllib.parse, urllib.error
import urllib.parse
import xbmcgui
import xbmcplugin
import urllib.request, urllib.error, urllib.parse
import tempfile
import os
#import m3u8
import re
from bs4 import BeautifulSoup


base_url = sys.argv[0]
addon_handle = int(sys.argv[1])
args = urllib.parse.parse_qs(sys.argv[2][1:])

xbmcplugin.setContent(addon_handle, 'movies')

def get_high_quality_stream(main_m3u8_uri):
    # manually download the main m3u8 to get the best quality link on the last line
    tmp_m3u8_file = tempfile.NamedTemporaryFile()
    main_m3u8_filename = tmp_m3u8_file.name
    urllib.request.urlretrieve(main_m3u8_uri, main_m3u8_filename)
    main_m3u8_file = open(main_m3u8_filename, 'rt')
    main_m3u8_content = main_m3u8_file.read()
    hq_video_uri = main_m3u8_uri
    use_next_line = True
    max_bandwidth = 0
    for line in main_m3u8_content.splitlines():
        if use_next_line:
            hq_video_uri = line
            use_next_line = False
            continue
        # Extract bandwidth info
        if line.startswith('#EXT-X-STREAM-INF'):
            for info in re.split(',|:',line):
                if info.startswith('BANDWIDTH='):
                    bw = int(info[10:])
                    if bw > max_bandwidth:
                        max_bandwidth = bw
                        use_next_line = True
    main_m3u8_file.close()
    return hq_video_uri

"""
def get_high_quality_stream(url):
    variant_m3u8 = m3u8.load(url)
    #variant_m3u8.dump('/tmp/playlist.m3u8')
    play_uri = url
    bandwidth = 0
    if variant_m3u8.is_variant:
        for playlist in variant_m3u8.playlists:
            if playlist.stream_info.bandwidth > bandwidth:
                play_uri = playlist.uri
                bandwidth = playlist.stream_info.bandwidth
    return play_uri
"""

def build_url(query):
    return base_url + '?' + urllib.parse.urlencode(query)

mode = args.get('mode', None)

if mode is None:
    # First the live feed
    li = xbmcgui.ListItem('Watch Kringvarp Live')
    li.setInfo('video', {'title': 'Watch Kringvarp Live', 'mediatype': 'video'})
    li.setArt({'icon': 'DefaultVideo.png'})
    li.setProperty('IsPlayable', 'true')
    url = build_url({'mode': 'live'})
    xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li)

    # Then a folder for programs
    url = build_url({'mode': 'programlist'})
    li = xbmcgui.ListItem('Sendingar')
    li.setArt({'icon': 'DefaultFolder.png'})
    xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li, isFolder=True)

    xbmcplugin.endOfDirectory(addon_handle)

elif mode[0] == 'programlist':
    # Get a list of programs from kvf.fo and add a folder per program
    url = 'http://kvf.fo/podcast'
    source = urllib.request.urlopen(url)
    # Turn the saved source into a BeautifulSoup object
    #soup = BeautifulSoup(source, convertEntities=BeautifulSoup.HTML_ENTITIES)
    soup = BeautifulSoup(source)
    for span in soup.findAll('span', {'class': ['field-content']}):
        title = span.text
        # By replacing http by rss XBMC can figure out to do lists automaticly
        feedurl = span.find('a')['href'].replace('http', 'rss')
        li = xbmcgui.ListItem(title)
        li.setArt({'icon': 'DefaultFolder.png'})
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=feedurl, listitem=li, isFolder=True)
    xbmcplugin.endOfDirectory(addon_handle)

elif mode[0] == 'live':
    # start the live feed
    url = 'https://play.kringvarp.fo/redirect/uttanlands/_definst_/smil:uttanlands.smil?type=m3u8'
    play_uri = get_high_quality_stream(url)
    play_item = xbmcgui.ListItem(path=play_uri)
    play_item.setContentLookup(False)
    play_item.setProperty('IsPlayable', 'true')
    xbmcplugin.setResolvedUrl(handle=addon_handle, succeeded=True, listitem=play_item)
